<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle;

/**
 * Description of Customer
 *
 * @author omar
 */
class Customer {
    //put your code here
       private $email;
       private $gender;
       private $fname;
       private $lname;
       private $country;
       private $bonusRate;
       private $basicBalance;
       private $bonusBalance;
       private $depositCount;
      /* Member functions */
      function setEmail($p){
         $this->email= $p;
      }
      
      function getEmail(){
         return $this->email ;
      }
      
      function setGender($p){
         $this->gender = $p;
      }
      
      function getGender(){
         return $this->gender;
      }
      
      function setFName($p){
         $this->fname = $p;
      }
      
      function getFName(){
         return $this->fname;
      }
      
      function setLName($p){
         $this->lname = $p;
      }
      
      function getLName(){
         return $this->lname;
      }
      
        function setBonusRate($p){
         $this->bonusRate = $p;
      }
      
      function getBonusRate(){
         return $this->bonusRate;
      }
        
      function setBasicBalance($p){
         $this->basicBalance = $p;
      }
      
      function getBasicBalance(){
         return $this->basicBalance;
      }
      
       
      function setBonusBalance($p){
         $this->bonusBalance = $p;
      }
      
      function getBonusBalance(){
         return $this->bonusBalance;
      }
       
      function setCountry($p){
         $this->country = $p;
      }
      
      function getCountry(){
         return $this->country;
      }
     
      function setDepositCount($p){
         $this->depositCount = $p;
      }
      
      function getDepositCount(){
         return $this->depositCount;
      }
      
      
      
}
