<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



namespace AppBundle\Controller;
 
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Customer;
use \PDO;

/**
 * Description of CustomerController
 *
 * @author omar
 */
class CustomerController extends FOSRestController{
    private static $instance = null;
   
        /**
     * @Rest\Get("/customer")
     */
    public function getAction()
    {
      $restresult =  self::getDBConnection()->query('SELECT * FROM customers');
              //$this->getDoctrine()->getRepository('AppBundle:User')->findAll();
        if ($restresult === null) {
          return new View("there are no customers exist", Response::HTTP_NOT_FOUND);
     }
        return $restresult->fetch();
    }
    
    
    
    /**
     * @Rest\Post("/addCustomer")
     */
    public function addCustomerAction(Request $request)
    {
            $customer = new Customer();
            $email = $request->get('email');


              if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
                 return new View("Email should be in a valid email format ", Response::HTTP_NOT_ACCEPTABLE); 

              }


            $customer->setEmail($email);
            $customer->setGender($request->get('gender'));
            $customer->setFName($request->get('fname'));
            $customer->setLName($request->get('lname'));
            $customer->setCountry($request->get('country'));
            $customer->setBasicBalance(0.0);
            $customer->setBonusBalance(0.0);
            $customer->setBonusRate(rand(5,20));
            $customer->setDepositCount(0);


           $statement = self::getDBConnection()->prepare("INSERT INTO customers(email,  gender,  fname,  lname,  country,  basicbalance,  bonusbalance,  bonusrate, depositCount)
                                                          VALUES              (:email, :gender, :fname, :lname, :country, :basicbalance, :bonusbalance, :bonusrate, :depositCount )");
           if($statement->execute(array(
                                       "email" => $customer->getEmail(),
                                       "gender" => $customer->getGender(),
                                       "fname" => $customer->getFName(),
                                       "lname" => $customer->getLName(),
                                       "country" => $customer->getCountry(),
                                       "basicbalance" => $customer->getBasicBalance(),
                                       "bonusbalance" => $customer->getBonusBalance(),
                                       "bonusrate" => $customer->getBonusRate(),
                                       "depositCount" => $customer->getDepositCount()

               )))
           {
               return new View("Customer has been Added Successfully", Response::HTTP_OK);

           }
           else  
             return new View("Customer registration  failed", Response::HTTP_NOT_FOUND);

    }
    
    
      /**
 * @Rest\Put("/editCustomer/{id}")
 */
    public function editCustomerAction($id,Request $request)
    {   
        $email = $request->get('email');
        $gender = $request->get('gender');
        $fname = $request->get('fname');
        $lname = $request->get('lname');
        $country = $request->get('country');

        
        
        $statement = self::getDBConnection()->prepare("UPDATE customers   
                                                       SET email = :email,
                                                           gender = :gender,
                                                           fname = :fname,
                                                           lname = :lname,
                                                           country = :country
                                                           WHERE id= :id  ");
             

        $statement->bindValue(":id", $id);
        $statement->bindValue(":email", $email);
        $statement->bindValue(":gender", $gender);
        $statement->bindValue(":fname", $fname);
        $statement->bindValue(":lname", $lname);
        $statement->bindValue(":country", $country);

        
          
        
        if ( $statement ->execute()) 
           return new View("Customer data has been updated Successfully", Response::HTTP_OK);
        else 
             return new View("edit failed", Response::HTTP_NOT_ACCEPTABLE);
 
    }
    
      /**
 * @Rest\Put("/depositToCustomer/{id}")
 */
    public function depositAction($id,Request $request)
    {   
        $email = $request->get('email');
        $amount = $request->get('amount');
        $pdo=self::getDBConnection();
        if($pdo->beginTransaction ())
        {
                    $statement = $pdo->prepare("UPDATE customers   
                                                                   SET basicbalance =basicbalance+ :amount,
                                                                       depositcount = (depositcount +1),
                                                                       bonusbalance =  case when depositcount % 3 = 0 then (bonusbalance + bonusrate)
                                                                       else bonusbalance
                                                                       end
                                                                       WHERE email = :email  and id= :id  ");

                    $statement->bindValue(":amount", $amount);
                    $statement->bindValue(":email", $email);
                    $statement->bindValue(":id", $id);

                    $statement3 = $pdo->prepare("insert into deposits( customerid, amount, dateofdeposit)  
                                                                                values   ( :id,       :amount,:dateofdeposit)");

                    $statement3->bindValue(":amount", $amount);
                    $statement3->bindValue(":id", $id);
                    $statement3->bindValue(":dateofdeposit", date('Y/m/d H:i:s'));


                    if ( $statement ->execute()&& $statement3->execute()) 
                    {
                        $pdo -> commit ();
                        return new View("deposit has been done Successfully", Response::HTTP_OK);
                    }
                    else 
                    {
                        $pdo -> rollback ();
                        return new View("deposit failed", Response::HTTP_NOT_ACCEPTABLE);
                    }
        }
        else
            return new View("deposit failed due to another transaction  being under process" , Response::HTTP_NOT_ACCEPTABLE);
 
    }
    
     /**
 * @Rest\Put("/withdrawFromCustomer/{id}")
 */
    public function withdrawAction($id,Request $request)
    {   
        $email = $request->get('email');
        $amount = $request->get('amount');
        $pdo=self::getDBConnection();
        if($pdo->beginTransaction ())
        {
            $statement =  $pdo->prepare('SELECT basicbalance FROM customers'
                . '                                      WHERE email = :email  and id= :id ');
       
                $statement->bindValue(":email", $email);
                $statement->bindValue(":id", $id);

                if($statement ->execute()  == false)
                      return new View("error retrieving customer data", Response::HTTP_NOT_FOUND);  ;

                $tempbalance =  $statement ->fetch(PDO::FETCH_OBJ)->basicbalance;
                if(  $tempbalance - $amount <= 0)
                      return new View("Customer balanace cannot be below zero ", Response::HTTP_NOT_ACCEPTABLE);  ;

                //echo "hello-->", $statement ->fetch(PDO::FETCH_OBJ)->basicbalance ;

                $statement2 = $pdo->prepare("UPDATE customers   
                                                               SET basicbalance = basicbalance - :amount
                                                               WHERE email = :email  and id= :id  ");

                $statement2->bindValue(":amount", $amount);
                $statement2->bindValue(":email", $email);
                $statement2->bindValue(":id", $id);


                $statement3 = $pdo->prepare("insert into withdraws( customerid, amount, dateofwithdraw)  
                                                                            values   ( :id,       :amount,:dateofwithdraw) ");

                $statement3->bindValue(":amount", $amount);
                $statement3->bindValue(":id", $id);
                $statement3->bindValue(":dateofwithdraw",date('Y/m/d H:i:s') );

                if ( $statement2 ->execute() &&  $statement3->execute())
                {
                    $pdo -> commit ();
                    return new View("withdraw has been   done Successfully  "   , Response::HTTP_OK);
                }
                else 
                {
                    $pdo -> rollback ();
                    return new View("Withdraw failed " , Response::HTTP_NOT_ACCEPTABLE);
                }
        }
        else
            return new View("Withdraw failed due to another transaction  being under process" , Response::HTTP_NOT_ACCEPTABLE);
    }
    
    
    /**
     * @Rest\Get("/generateReport/{noOfDays}")
     */
    public function generateReportAction( $noOfDays  )
    {
       //$restresult =  self::getDBConnection()->query('SELECT * FROM report');
        $statement =  self::getDBConnection()->prepare('CALL generateReport(:noOfDays)');

        $statement->bindValue(":noOfDays", $noOfDays);     
        
        if ($statement ->execute()  == FALSE) {
          return new View("report could not be generated", Response::HTTP_NOT_FOUND);
     }
        return $statement->fetchAll();
        
 /*
  *  I have chosen to use a stored procedure as it is easier to parse such a report within the database engine and get the results back
  * here is the query used to built the stored procedure
  *  SELECT 
        CAST(NOW() AS DATE) AS `date(NOW())`,
        `g`.`country` AS `country`,
        (SELECT 
                COUNT(`b`.`id`)
            FROM
                (`simonbell`.`customers` `a`
                JOIN `simonbell`.`deposits` `b` ON ((`a`.`id` = `b`.`customerid`)))
            WHERE
                ((`b`.`dateofdeposit` >= (CAST(NOW() AS DATE) - INTERVAL 7 DAY))
                    AND (`a`.`country` = `g`.`country`))) AS `NoOfdeposits`,
        (SELECT 
                SUM(`b`.`amount`)
            FROM
                (`simonbell`.`customers` `a`
                JOIN `simonbell`.`deposits` `b` ON ((`a`.`id` = `b`.`customerid`)))
            WHERE
                ((`b`.`dateofdeposit` >= (CAST(NOW() AS DATE) - INTERVAL 7 DAY))
                    AND (`a`.`country` = `g`.`country`))) AS `deposittotal`,
        (SELECT 
                COUNT(`f`.`id`)
            FROM
                (`simonbell`.`customers` `a`
                JOIN `simonbell`.`withdraws` `f` ON ((`a`.`id` = `f`.`customerid`)))
            WHERE
                ((`f`.`dateofwithdraw` >= (CAST(NOW() AS DATE) - INTERVAL 7 DAY))
                    AND (`a`.`country` = `g`.`country`))) AS `NoOfWithdraws`,
        (SELECT 
                SUM(`f`.`amount`)
            FROM
                (`simonbell`.`customers` `a`
                JOIN `simonbell`.`withdraws` `f` ON ((`a`.`id` = `f`.`customerid`)))
            WHERE
                ((`f`.`dateofwithdraw` >= (CAST(NOW() AS DATE) - INTERVAL 7 DAY))
                    AND (`a`.`country` = `g`.`country`))) AS `withdrawatotal`
    FROM
        `simonbell`.`customers` `g`
    GROUP BY `g`.`country`
    HAVING ((`deposittotal` > 0)
        OR (`withdrawatotal` > 0))
  * 
  * 
  */
    }
    
    
    public static function getDBConnection()
   {
       if(self::$instance == null)
       {
            $dsn = 'mysql:host=localhost;dbname=simonbell';
            $username = 'root';
            $password = 'toor';
            $options = array(
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            ); 
            try {
                self::$instance  = new PDO($dsn, $username, $password, $options);
            } catch (PDOException $e) {
                echo 'Connection failed: ' . $e->getMessage();
            }
            
       }
       return self::$instance;
   }
   
}
