<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Controller;

/**
 * Description of DB
 *
 * @author omar
 */
class DB {
    private static $instance = null;
   public static function get()
   {
       if(self::$instance == null)
       {
           $dsn = 'mysql:host=localhost;dbname=simonbell';
            $username = 'root';
            $password = 'toor';
            $options = array(
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            ); 
            try {
                self::$instance  = new PDO($dsn, $username, $password, $options);
            } catch (PDOException $e) {
                echo 'Connection failed: ' . $e->getMessage();
            }
            
       }
       return self::$instance;
   }
}
